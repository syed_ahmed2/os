#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#define BUFSIZE 10
int main() {

  char buffin[BUFSIZE] = "empty";
  char buffout[] = "hello";
  int bytesin;
  int fd[2];
  pid_t childpid;

  if (pipe(fd) == -1) {
    fprintf(stderr, "Pipe Failed");
    return 1;
  }
  bytesin = strlen(buffin);

  childpid = fork();

  if (childpid < 0) {
    fprintf(stderr, "fork Failed");
    return 1;
  }
  if (childpid <= 0)
    write(fd[1], buffout, strlen(buffout) + 1);

  else
    bytesin = read(fd[0], buffin, BUFSIZE);
  fprintf(stderr, "[%ld]: my buffin is {%.*s}, my buffout is {%s}\n",
          (long) getpid(), bytesin, buffin, buffout);

  return 0;
}
