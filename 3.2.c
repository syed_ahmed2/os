#include <sys/types.h>
#include "stdio.h"
#include <unistd.h>
int globalvariable =2;

int main() {
  char parentStr[] = "Parent Process";
  char childStr[] = "Child Process";
  char *string = NULL;
  int functionVariable = 20;
  pid_t pid=fork();
  if(pid<0){
    perror("unable to create child process");
    return 1;
  }else if(pid==0){
    string =&childStr[0];
    globalvariable++;
    functionVariable++;
  }else if(pid>0){
    string =&parentStr[0];
    globalvariable+=2;
    functionVariable+=2;
  }
  printf("%s\n",string);
  printf("Global Variable :%d\n",globalvariable);
  printf("Function Variable :%d\n",functionVariable);
  return 0;
}
