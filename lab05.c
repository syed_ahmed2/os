#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <semaphore.h> 
int matrix1[256][256], matrix2[256][256], result[256][256];
sem_t mutex;

int matrixOutput(int matrix[256][256]){
  FILE *fp=fopen("output.txt", "w+");
  if(fp==NULL){
    printf("could not open file!\n");
    exit(-1);
  }

  for(int i=0; i<256; i++){
    for(int j=0; j<256; j++){
      fprintf(fp,"%d ", matrix[i][j]);	
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

void* matrixAdd(void *i){		
  int num=(int)i;

  for(int i=num*32; i<num*32+31; i++){
    for(int j=0; j<256; j++){
      result[i][j]=matrix1[i][j]+matrix2[i][j];
    }
  }

  sem_wait(&mutex);
  printf("Thread %d Done!\n", num);
  sem_post(&mutex);
  pthread_exit(NULL);
}

int main(){
  sem_init(&mutex, 0, 1);

  for(int i=0; i<256; i++){
    for(int j=0; j<256; j++){
      matrix1[i][j]=j+1;
      matrix2[i][j]=j+1;
      result[i][j]=0;
    }
  }

  pthread_t thread[8];	
  for(int i=7; i>=0; i--) { 
    pthread_create(&thread[i], NULL, matrixAdd, (void*)i);
  }

  for(int i=7; i>=0; i--) { 
    pthread_join(thread[i], NULL);
  }

  matrixOutput(result);

  return 0;
}
