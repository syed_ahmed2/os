#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
int main(void){
  pid_t childpid;
  int fd[2];
  if(pipe(fd) == -1){ 
    perror("Failed to setup pipeline");
    return 1;
  }
  if((childpid = fork()) == -1){ 
    perror("Failed to fork a child");
    return 1;
  }
  if(childpid == 0){ 
    if(dup2(fd[1],STDOUT_FILENO)==-1)
      perror("Failed to redirect stdout of env");
    else if(close(fd[0] == -1)) 
      perror("Failed to close extra pipe descriptors on env");
    else{
      execl("/usr/bin/env", "env", NULL); 
      perror("Failed to exec env");
    }
    return 1;
  }
  if(dup2(fd[0],STDIN_FILENO)==-1) 
    perror("Failed to redirect stdin of grep");

  else{
    execl("/bin/grep", "grep", "HOME", NULL); 
    perror("Failed to exec grep");
  }
  return 1;
}
